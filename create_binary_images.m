% create binary images


% folder_imgs = 'C:\Users\ffuentes.I3BH\Documents\DOCTORADO\01_DATA\_common_dbs\cfd-boy-neutral-allraces\eyes-without-mask\same-size\';
folder_imgs = 'test-images\';
extension = '.bmp'; %'.jpg';

% list all 'extension' files in 'folder_imgs'
img_filenames = dir([folder_imgs 'CFD' '*' extension]); % 'CFD' to avoid masks_
n_img_filenames = numel(img_filenames);

for nf=1:n_img_filenames
    
    % % Image filename
    filename = strrep(img_filenames(nf).name, extension, '');
    
    ia = imread(['homogenized_median64\' filename '.bmp']);
    ib = imread(['homogenized_median128\' filename '.bmp']);
    id = ia-ib;

    figure;
    subplot(2,1,1), imagesc(id);colorbar
    subplot(2,1,2), imshow(logical(id))
end