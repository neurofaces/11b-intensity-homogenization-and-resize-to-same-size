% try to homogenize images' intensity

% run_face_parameterization

pri = priority('h');

folder_imgs = 'C:\Users\ffuentes.I3BH\Documents\DOCTORADO\01_DATA\_common_dbs\cfd-boy-neutral-allraces\eyes-without-mask\same-size\';
folder_results = fullfile(folder_imgs, 'homogenized_median128\');
if(~exist(folder_results, 'dir')), mkdir(folder_results); end;

extension = '.bmp'; %'.jpg';

% list all 'extension' files in 'folder_imgs'
img_filenames = dir([folder_imgs 'CFD' '*' extension]); % 'CFD' to avoid masks_
n_img_filenames = numel(img_filenames);


hw = waitbar(0, 'Computing histogram of all images... 0%');
t=tic;

Hgrams256 = zeros(n_img_filenames, 256);
Hgrams128 = zeros(n_img_filenames, 128);
Hgrams64 = zeros(n_img_filenames, 64);
Hgrams32 = zeros(n_img_filenames, 32);
for nf=1:n_img_filenames
    waitbar(nf/n_img_filenames, hw, sprintf('Computing histogram of all images... %.2f%%', nf/n_img_filenames*100));
    
    % % Image filename
    filename = strrep(img_filenames(nf).name, extension, '');
    
    % % Load image
    img = imread(fullfile(folder_imgs, [filename extension]));
    img_gray = rgb2gray(img);
    
    % % Histograms
    Hgrams256(nf,:) = imhist(img_gray, 256);
    Hgrams128(nf,:) = imhist(img_gray, 128);
    Hgrams64(nf,:) = imhist(img_gray, 64);
    Hgrams32(nf,:) = imhist(img_gray, 32);
end
Hgram256mean = mean(Hgrams256, 1);
Hgram128mean = mean(Hgrams128, 1);
Hgram64mean = mean(Hgrams64, 1);
Hgram32mean = mean(Hgrams32, 1);
Hgram256median = median(Hgrams256, 1);
Hgram128median = median(Hgrams128, 1);
Hgram64median = median(Hgrams64, 1);
Hgram32median = median(Hgrams32, 1);
Hgram256mode = mode(Hgrams256, 1);
Hgram128mode = mode(Hgrams128, 1);
Hgram64mode = mode(Hgrams64, 1);
Hgram32mode = mode(Hgrams32, 1);

figure; 
subplot(4,3,1), bar(Hgram256mean), title('256 mean')
subplot(4,3,2), bar(Hgram256median), title('256 median')
subplot(4,3,3), bar(Hgram256mode), title('256 mode')
subplot(4,3,4), bar(Hgram128mean), title('128 mean')
subplot(4,3,5), bar(Hgram128median), title('128 median')
subplot(4,3,6), bar(Hgram128mode), title('128 mode')
subplot(4,3,7), bar(Hgram64mean), title('64 mean')
subplot(4,3,8), bar(Hgram64median), title('64 median')
subplot(4,3,9), bar(Hgram64mode), title('64 mode')
subplot(4,3,10), bar(Hgram32mean), title('32 mean')
subplot(4,3,11), bar(Hgram32median), title('32 median')
subplot(4,3,12), bar(Hgram32mode), title('32 mode')
close(hw);

% start the loop for every image in the list to homogenize images
hw = waitbar(0, 'Homogenizing images... 0%');
for nf=1:n_img_filenames
    
    waitbar(nf/n_img_filenames, hw, sprintf('Homogenizing images... %.2f%%', nf/n_img_filenames*100));
    cprintf('blue', ['Homogenizing image ' num2str(nf) '/' num2str(n_img_filenames) '\n']);
    
    % % Image filename
    filename = strrep(img_filenames(nf).name, extension, '');
    
    % % Load image
    img = imread(fullfile(folder_imgs, [filename extension]));
    img_gray = rgb2gray(img);
    
    % % Load mask
    % mask = imread(fullfile(folder_masks, [strrep(filename, '_L', '_mask_LE') extension]));
    
    % % Homogenize
    imgh = histeq(img_gray, Hgram128median); 
    
    % figure, subplot(2,1,1), imshow(imgh);
    % subplot(2,1,2), imshow(img_gray), title('original');
        
    % % Save homogenized image   
    imwrite(imgh, fullfile(folder_results, strcat(filename, '.bmp')));

end
close(hw);


% % Checking if histogram adjustment worked
hw = waitbar(0, 'Checking adjusted histograms... 0%');
Hgrams256adj = zeros(n_img_filenames, 256);
Hgrams128adj = zeros(n_img_filenames, 128);
Hgrams64adj = zeros(n_img_filenames, 64);
Hgrams32adj = zeros(n_img_filenames, 32);
for nf=1:n_img_filenames
    waitbar(nf/n_img_filenames, hw, sprintf('Checking adjusted histograms... %.2f%%', nf/n_img_filenames*100));
    
    % % Image filename
    filename = strrep(img_filenames(nf).name, extension, '');
    
    % % Load image
    img = imread(fullfile(folder_results, [filename extension]));
    
    % % Histograms
    Hgrams256adj(nf,:) = imhist(img, 256);
    Hgrams128adj(nf,:) = imhist(img, 128);
    Hgrams64adj(nf,:) = imhist(img, 64);
    Hgrams32adj(nf,:) = imhist(img, 32);
end

Hgram256meanadj = mean(Hgrams256adj, 1);
Hgram128meanadj = mean(Hgrams128adj, 1);
Hgram64meanadj = mean(Hgrams64adj, 1);
Hgram32meanadj = mean(Hgrams32adj, 1);
Hgram256medianadj = median(Hgrams256adj, 1);
Hgram128medianadj = median(Hgrams128adj, 1);
Hgram64medianadj = median(Hgrams64adj, 1);
Hgram32medianadj = median(Hgrams32adj, 1);
Hgram256modeadj = mode(Hgrams256, 1);
Hgram128modeadj = mode(Hgrams128, 1);
Hgram64modeadj = mode(Hgrams64, 1);
Hgram32modeadj = mode(Hgrams32, 1);

close(hw);

figure; title('Adjusted histograms');
subplot(4,3,1), bar(Hgram256meanadj), title('256 mean')
subplot(4,3,2), bar(Hgram256medianadj), title('256 median')
subplot(4,3,3), bar(Hgram256modeadj), title('256 mode')
subplot(4,3,4), bar(Hgram128meanadj), title('128 mean')
subplot(4,3,5), bar(Hgram128medianadj), title('128 median')
subplot(4,3,6), bar(Hgram128modeadj), title('128 mode')
subplot(4,3,7), bar(Hgram64meanadj), title('64 mean')
subplot(4,3,8), bar(Hgram64medianadj), title('64 median')
subplot(4,3,9), bar(Hgram64modeadj), title('64 mode')
subplot(4,3,10), bar(Hgram32meanadj), title('32 mean')
subplot(4,3,11), bar(Hgram32medianadj), title('32 median')
subplot(4,3,12), bar(Hgram32modeadj), title('32 mode')

toc(t)

priority(pri);