% study images histogram

folder_imgs = 'C:\Users\ffuentes.I3BH\Documents\DOCTORADO\01_DATA\_common_dbs\cfd-boy-neutral-allraces\eyes-without-mask\same-size\';
folder_imgs = 'test-images\';
extension = '.bmp'; %'.jpg';

% list all 'extension' files in 'folder_imgs'
img_filenames = dir([folder_imgs 'CFD' '*' extension]); % 'CFD' to avoid masks_
n_img_filenames = numel(img_filenames);

% Load ref image
Ref = rgb2gray(imread([folder_imgs 'REF.bmp']));
LevelsRef = unique(Ref(:));
disp(['image REF: # levels: ' num2str(length(LevelsRef))]);
disp(['max level = ' num2str( max(LevelsRef) )]);
disp(['min level = ' num2str( min(LevelsRef) )]);
    
% Scale it to full 8-bit range
nbits = 2^8-1;
Refdouble = double(Ref);                % cast uint8 to double
kmult = nbits/(max(max(Refdouble(:)))); % full range multiplier
Ref = uint8(kmult*Refdouble);           % full range 8-bit reference image
    
for nf=1:n_img_filenames
    
    % % Image filename
    filename = strrep(img_filenames(nf).name, extension, '');
    
    % % Load image
    img = imread(fullfile(folder_imgs, [filename extension]));
    
    % % rgb2gray
    K = rgb2gray(img);
    
    % % Image study
    % LevelsK = unique(K(:));
    % disp(['image K: # levels: ' num2str(length(LevelsK))]);
    % disp(['max level = ' num2str( max(LevelsK) )]);
    % disp(['min level = ' num2str( min(LevelsK) )]);
    % 
    % % Scale it to full 8-bit range
    % Kdouble = double(K);                  % cast uint8 to double
    % kmult = nbits/(max(max(Kdouble(:)))); % full range multiplier
    % Ref = uint8(kmult*Kdouble);   % full range 8-bit reference image
    
        
    % % Histogram
    figure; 
    subplot(1,2,1), imhist(K)
    title('K: Image to homogenize');
    subplot(1,2,2), imhist(Ref)
    title('Ref: Reference Image');
    
    B8bit64 = imhistmatch(K(:,:,1),Ref(:,:,1));  % default # bins: N = 64
    N = length(LevelsRef);      % number of unique 8-bit code values in image A
    B8bitUniq = imhistmatch(K(:,:,1),Ref(:,:,1),N);
    figure;
    subplot(2,2,1), imshow(B8bit64)
    title('B16bit64: N = 64');
    subplot(2,2,2), imshow(B8bitUniq)
    title('B16bitUniq: N = 448')
    subplot(2,2,3), imshow(Ref)
    subplot(2,2,4), imshow(K)
end